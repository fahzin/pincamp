import React, {useState} from 'react';
import {
  citiesList,
  verifyInput,
  verifyIfFormHasErrors,
} from '../../shared/utils';
import Input from '../../components/Form/Input/Input';
import Button from '../../components/Form/Button/Button';
import Message from '../../components/Message/Message';

const UserDataForm = () => {
  const [formStatus, setFormStatus] = useState({
    isSent: false,
    status: 'idle',
    message: '',
  });

  const [fieldsData, setFieldsData] = useState({
    name: {
      label: 'Name',
      value: '',
      name: 'name',
      type: 'text',
      placeholder: 'Please, enter your full name',
      isRequired: true,
      error: false,
      errorMessage: '',
      validation: ['name'],
    },
    address: {
      label: 'Address',
      value: '',
      name: 'address',
      type: 'text',
      placeholder: 'Please, enter your address',
      isRequired: true,
      error: false,
      errorMessage: '',
      validation: ['text'],
    },
    zipcode: {
      label: 'Zipcode',
      value: '',
      name: 'zipcode',
      type: 'text',
      placeholder: 'Please, enter your zipcode',
      isRequired: true,
      error: false,
      errorMessage: '',
      validation: ['zipcode', 'number'],
    },
    city: {
      label: 'City',
      value: '',
      name: 'city',
      type: 'text',
      placeholder: 'Please, enter your city',
      isRequired: true,
      error: false,
      errorMessage: '',
      list: 'cities',
      options: [...citiesList],
      isOpen: false,
      validation: ['text'],
    },
  });

  const onSelectOption = (value, fieldName) => {
    // Update value of the field to autocomplete
    setFieldsData(prevState => ({
      ...prevState,
      [fieldName]: {
        ...prevState[fieldName],
        error: false,
        errorMessage: '',
        value,
        isOpen: false,
      },
    }));
  };

  // Function to handle close options Box if field has it
  const handleOnBlur = ({target: {name: fieldName}}) => {
    setFieldsData(prevState => ({
      ...prevState,
      [fieldName]: {
        ...prevState[fieldName],
        isOpen: false,
      },
    }));
  };

  // Function to handle the updated value of each field
  const handleOnChange = ({target: {value, name: fieldName}}) => {
    const {options, isRequired, validation} = fieldsData[fieldName];
    const {error, errorMessage, extraInfo} = verifyInput({
      value,
      isRequired,
      validation,
    });

    // If field has options for autocomplete show them while typing
    const isOpen = options && options.length > 0;

    // If extraInfo has a cityFound property, autofill city field if it exits
    if (extraInfo && extraInfo.cityFound && fieldsData.city) {
      onSelectOption(extraInfo.cityFound, 'city');
    }

    // Use prevState to prevent it to be overwritten
    setFieldsData(prevState => ({
      ...prevState,
      [fieldName]: {
        ...prevState[fieldName],
        value,
        error,
        errorMessage,
        isOpen,
      },
    }));
  };

  // Open options box (if exists) on input focus
  const handleOnFocus = ({target: {name: fieldName}}) => {
    if (fieldsData[fieldName].options) {
      setFieldsData(prevState => ({
        ...prevState,
        [fieldName]: {
          ...prevState[fieldName],
          isOpen: true,
        },
      }));
    }
  };

  const clearForm = () => {
    const fieledsCleared = {...fieldsData};

    for (const key in fieldsData) {
      if (fieldsData.hasOwnProperty(key)) {
        // set value to empty and blur the field
        fieledsCleared[key].value = '';
      }
    }

    // Blur field if focused after value cleaning
    if ('activeElement' in document) document.activeElement.blur();

    setFieldsData(fieledsCleared);
  };

  const handleSubmit = evt => {
    // Prevent page reload on submit form
    evt.preventDefault();
    let formHasErrors = false;

    for (const key in fieldsData) {
      if (fieldsData.hasOwnProperty(key)) {
        // Get error status from each field while also display errors if there's any
        const {value, isRequired, validation} = fieldsData[key];
        const {error} = verifyInput({
          value,
          isRequired,
          validation,
        });

        if (error) {
          formHasErrors = true;
        }
      }
    }

    // Prevent user to remove 'disabled' property from the DOM and
    // send a empty form
    if (formHasErrors) {
      setFormStatus(prevState => ({
        ...prevState,
        isSent: true,
        status: 'error',
        message: 'Please, fix the errors on your data first.',
      }));

      return;
    }

    clearForm();
    setFormStatus(prevState => ({
      ...prevState,
      isSent: true,
      status: 'success',
      message: "Thank you for your data, we'll get back to you soon.",
    }));
  };

  return (
    <form onSubmit={handleSubmit}>
      <h1>User Data Form</h1>
      {formStatus.isSent && (
        <Message
          messageStatus={formStatus.status}
          message={formStatus.message}
        />
      )}
      {Object.keys(fieldsData).map(key => (
        <Input
          value={fieldsData[key].value}
          error={fieldsData[key].error}
          errorMessage={fieldsData[key].errorMessage}
          key={fieldsData[key].name}
          label={fieldsData[key].label}
          name={fieldsData[key].name}
          type={fieldsData[key].type}
          placeholder={fieldsData[key].placeholder}
          onChange={handleOnChange}
          onBlur={handleOnBlur}
          onSelectOption={onSelectOption}
          onFocus={handleOnFocus}
          options={fieldsData[key].options}
          isOpen={fieldsData[key].isOpen}
          isRequired={fieldsData[key].isRequired}
        />
      ))}
      <Button
        type="submit"
        text={'Send'}
        disabled={verifyIfFormHasErrors(fieldsData)}
      />
    </form>
  );
};

export default UserDataForm;
