import React from 'react';
import {mount} from 'enzyme';
import Message from '../../components/Message/Message';
import UserDataForm from './UserDataForm';

describe('UserDataForm', () => {
  const wrapper = mount(<UserDataForm />);

  it('should match the snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('should have a name field', () => {
    expect(wrapper.exists('input[name="name"]')).toEqual(true);
  });

  it('should have proper props for name', () => {
    expect(wrapper.find('input[name="name"]').props()).toEqual({
      value: '',
      onChange: expect.any(Function),
      onBlur: expect.any(Function),
      name: 'name',
      type: 'text',
      onFocus: expect.any(Function),
      onKeyDown: expect.any(Function),
      className: expect.any(String),
    });
  });

  it('should have an address field', () => {
    expect(wrapper.exists('input[name="address"]')).toEqual(true);
  });

  it('should have proper props for address', () => {
    expect(wrapper.find('input[name="address"]').props()).toEqual({
      value: '',
      onChange: expect.any(Function),
      onBlur: expect.any(Function),
      name: 'address',
      type: 'text',
      onFocus: expect.any(Function),
      onKeyDown: expect.any(Function),
      className: expect.any(String),
    });
  });

  it('should have a zipcode field', () => {
    expect(wrapper.exists('input[name="zipcode"]')).toEqual(true);
  });

  it('should have proper props for zipcode', () => {
    expect(wrapper.find('input[name="zipcode"]').props()).toEqual({
      value: '',
      onChange: expect.any(Function),
      onBlur: expect.any(Function),
      name: 'zipcode',
      type: 'text',
      onFocus: expect.any(Function),
      onKeyDown: expect.any(Function),
      className: expect.any(String),
    });
  });

  it('should have a city field', () => {
    expect(wrapper.exists('input[name="city"]')).toEqual(true);
  });

  it('should have proper props for city', () => {
    expect(wrapper.find('input[name="city"]').props()).toEqual({
      value: '',
      onChange: expect.any(Function),
      onBlur: expect.any(Function),
      name: 'city',
      type: 'text',
      onFocus: expect.any(Function),
      onKeyDown: expect.any(Function),
      className: expect.any(String),
    });
  });

  it('should have a button', () => {
    expect(wrapper.exists('button')).toEqual(true);
  });

  it('should have proper props for button', () => {
    expect(wrapper.find('button').props()).toEqual({
      disabled: true,
      children: 'Send',
      className: expect.any(String),
      type: 'submit',
    });
  });

  it('should set the name value on change event', () => {
    wrapper.find('input[name="name"]').simulate('change', {
      target: {
        value: 'Fabio',
        name: 'name',
      },
    });
    expect(wrapper.find('input[name="name"]').prop('value')).toEqual('Fabio');
  });

  it('should set the address value on change event', () => {
    wrapper.find('input[name="address"]').simulate('change', {
      target: {
        value: 'Some Street',
        name: 'address',
      },
    });
    expect(wrapper.find('input[name="address"]').prop('value')).toEqual(
      'Some Street'
    );
  });

  it('should show errors if send form with errors', () => {
    wrapper.find('form').simulate('submit');
    expect(
      wrapper.contains(
        <Message
          messageStatus={'error'}
          message={'Please, fix the errors on your data first.'}
        />
      )
    ).toEqual(true);
  });

  it('should set the zipcode value on change event', () => {
    wrapper.find('input[name="zipcode"]').simulate('change', {
      target: {
        value: '13089',
        name: 'zipcode',
      },
    });

    expect(wrapper.find('input[name="zipcode"]').prop('value')).toEqual(
      '13089'
    );
  });

  it('should focus on zipcode field', () => {
    wrapper.find('input[name="zipcode"]').simulate('focus');
  });

  it('should blur on zipcode field', () => {
    wrapper.find('input[name="zipcode"]').simulate('blur');
  });

  it('should change value of city to "Berlin"', () => {
    expect(wrapper.find('input[name="city"]').prop('value')).toEqual('Berlin');
  });

  it('should NOT have a list of options', () => {
    expect(wrapper.exists('ul')).toEqual(false);
  });

  it('should set the city value to "pots" on change event', () => {
    wrapper.find('input[name="city"]').simulate('change', {
      target: {
        value: 'pots',
        name: 'city',
      },
    });
  });

  it('should have a list of options', () => {
    expect(wrapper.exists('ul')).toEqual(true);
  });

  it('should select first item of the list and change city value to "Potsdam"', () => {
    wrapper
      .find('li')
      .first()
      .simulate('mousedown');

    expect(wrapper.find('input[name="city"]').prop('value')).toEqual('Potsdam');
  });

  it('should close list of options', () => {
    expect(wrapper.exists('ul')).toEqual(false);
  });

  it('should focus on city field', () => {
    wrapper.find('input[name="city"]').simulate('focus');
  });

  it('should open list of options', () => {
    expect(wrapper.exists('ul')).toEqual(true);
  });

  it('should blur on city field', () => {
    wrapper.find('input[name="city"]').simulate('blur');
  });

  it('should close list of options', () => {
    expect(wrapper.exists('ul')).toEqual(false);
  });

  it('should have disabled FALSE props for button and Send form', () => {
    expect(wrapper.find('button').props()).toEqual({
      disabled: false,
      children: 'Send',
      className: expect.any(String),
      type: 'submit',
    });
    wrapper.find('form').simulate('submit');
  });
});
