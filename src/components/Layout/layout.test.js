import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';
import Layout from './Layout';

describe('Layout', () => {
  it('renders without crashing given the required props', () => {
    const wrapper = shallow(
      <Layout>
        <>Mock Container</>
      </Layout>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
