import styled from 'styled-components';

export const MainWrapper = styled.div`
  width: 100vw;
  min-height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 15px;
`;

export const ContentWrapper = styled.div`
  width: 100%;
  max-width: 500px;
`;

export const Logo = styled.div`
  width: 100%;
  float: left;
  text-align: center;
  margin-bottom: 30px;

  img {
    height: 100px;
    width: auto;
  }
`;

export const ContentCard = styled.div`
  width: 100%;
  box-shadow: 0 1px 8px 0 rgba(0, 0, 0, 0.3);
  background-color: #ffffff;
  padding: 15px;
  float: left;
`;
