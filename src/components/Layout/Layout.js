import React from 'react';
import PropTypes from 'prop-types';
import logo from '../../assets/images/logo.svg';
import * as S from './layout.styled';

const Layout = ({children}) => (
  <S.MainWrapper>
    <S.ContentWrapper>
      <S.Logo>
        <img src={logo} alt="PiNCAMP Logo" title="PiNCAMP" />
      </S.Logo>
      <S.ContentCard>{children}</S.ContentCard>
    </S.ContentWrapper>
  </S.MainWrapper>
);

Layout.propTypes = {
  children: PropTypes.object.isRequired,
};

export default Layout;
