import styled from 'styled-components';

export const StyledMessage = styled.div`
  width: 100%;
  float: left;
  padding: 10px;
  margin-bottom: 15px;
  border-radius: 2px;
  color: ${({messageStatus}) =>
    messageStatus === 'success' ? 'rgb(71, 162, 61)' : 'rgb(217, 42, 45)'};
  background-color: ${({messageStatus}) =>
    messageStatus === 'success' ? 'rgb(208, 240, 209)' : 'rgb(240, 208, 209)'};
`;
