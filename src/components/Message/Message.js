import React from 'react';
import PropTypes from 'prop-types';
import * as S from './message.styled';

const Message = ({messageStatus, message}) => (
  <S.StyledMessage messageStatus={messageStatus}>{message}</S.StyledMessage>
);

Message.propTypes = {
  messageStatus: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
};

export default Message;
