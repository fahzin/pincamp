import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';
import Message from './Message';

describe('Message', () => {
  it('renders without crashing given the required props', () => {
    const props = {
      messageStatus: 'error',
      message: 'Mock Error message',
    };
    const wrapper = shallow(<Message {...props} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
