import React from 'react';
import {mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import InputSuggestions from './InputSuggestions';

describe('InputSuggestions', () => {
  const props = {
    options: [
      [10319, 'Berlin'],
      [10365, 'Berlin'],
      [10367, 'Berlin'],
    ],
    onSelectOption: () => {},
    currentOption: 0,
    fieldName: 'city',
  };

  const wrapper = mount(<InputSuggestions {...props} />);

  it('renders without crashing given the required props', () => {
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('should select option on user press Enter', () => {
    wrapper
      .find('li')
      .first()
      .simulate('focus');

    wrapper
      .find('li')
      .first()
      .simulate('keypress', {key: 'Enter'});
  });
});
