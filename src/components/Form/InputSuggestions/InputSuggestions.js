import React from 'react';
import PropTypes from 'prop-types';
import * as S from './inputSuggestions.styled';

const InputSuggestions = ({
  options,
  onSelectOption,
  currentOption,
  fieldName,
}) => (
  <S.List aria-expanded="true" role="listbox" tabIndex="0">
    {options.map((opt, index) => (
      <S.StyledOption
        key={`${opt[0]}_${opt[1]}`}
        // onMouseDown fires before onBlur, preventing options to close before it's clicked
        onMouseDown={() => onSelectOption(opt[1], fieldName)}
        onKeyPress={() => onSelectOption(opt[1], fieldName)}
        role="option"
        aria-selected={currentOption === index}
        selected={currentOption === index}
        id={`option_${index}`}
      >
        {opt[1]}
      </S.StyledOption>
    ))}
  </S.List>
);

InputSuggestions.propTypes = {
  options: PropTypes.array.isRequired,
  onSelectOption: PropTypes.func.isRequired,
  currentOption: PropTypes.number.isRequired,
  fieldName: PropTypes.string.isRequired,
};

export default InputSuggestions;
