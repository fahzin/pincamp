import styled from 'styled-components';

export const List = styled.ul`
  width: 100%;
  position: absolute;
  left: 0;
  top: 58px;
  background: white;
  max-height: 250px;
  overflow-x: hidden;
  overflow-y: auto;
  box-shadow: 0 1px 8px 0 rgba(0, 0, 0, 0.15);
  padding: 0;
  z-index: 10;
`;

export const StyledOption = styled.li`
  list-style: none;
  padding: 15px;
  cursor: pointer;
  transition: background-color 0.3s;

  background-color: ${({selected}) =>
    selected ? 'rgba(0, 0, 0, 0.05)' : 'transparend'};

  &:hover {
    background-color: rgba(0, 0, 0, 0.05);
  }
`;
