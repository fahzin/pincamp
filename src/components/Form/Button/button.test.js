import React from 'react';
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';
import Button from './Button';

describe('Button', () => {
  it('renders without crashing given the required props', () => {
    const props = {
      text: 'Send',
    };
    const wrapper = shallow(<Button {...props} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
