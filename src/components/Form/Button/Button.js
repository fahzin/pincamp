import React from 'react';
import PropTypes from 'prop-types';
import * as S from './button.styled';

const Button = ({text, ...rest}) => (
  <S.StyledButton {...rest}>{text}</S.StyledButton>
);

Button.propTypes = {
  text: PropTypes.string.isRequired,
};

export default Button;
