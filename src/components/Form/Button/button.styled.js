import styled from 'styled-components';

export const StyledButton = styled.button`
  display: inline-block;
  text-align: center;
  font-weight: 600;
  width: 100%;
  cursor: pointer;
  font-size: 16px;
  line-height: 1;
  text-decoration: none;
  border-radius: 8px;
  padding: 10px 14px;
  border-width: 2px;
  border-style: solid;
  border-color: transparent;
  border-image: initial;
  background-color: rgb(102, 178, 228);
  color: rgb(255, 255, 255);
  border-color: rgb(102, 178, 228);
  transition: 0.3s background-color, 0.3s border-color;

  &:hover {
    background-color: rgb(3, 71, 112);
    border-color: rgb(3, 71, 112);
  }
`;
