import React, {useState, useEffect, useMemo} from 'react';
import PropTypes from 'prop-types';
import {filterArrayByQuery} from '../../../shared/utils';
import InputSuggestions from '../InputSuggestions/InputSuggestions';
import * as S from './input.styled';

const Input = ({
  error,
  errorMessage,
  label,
  onChange,
  onBlur,
  name: fieldName,
  type,
  value,
  options,
  onSelectOption,
  isOpen,
  onFocus,
  isRequired,
}) => {
  const [currentOption, setCurrentOption] = useState(0);

  // Memoize filter to improve performance
  const filteredOptions = useMemo(() => filterArrayByQuery(value, options), [
    value,
    options,
  ]);

  const scrollToElement = () => {
    // Prevent jest from breaking test due to not having access to scrollIntoView
    if (typeof document.body.scrollIntoView === 'function') {
      document.getElementById(`option_${currentOption}`).scrollIntoView({
        behavior: 'smooth',
        block: 'center',
        inline: 'center',
      });
    }
  };

  const onKeyDown = evt => {
    // Perform action only if options exists
    if (options.length < 1 || value === '' || !isOpen) return;

    // User pressed Enter Key
    if (evt.keyCode === 13) {
      // Prevent form to be sent on Enter key pressed
      evt.preventDefault();
      setCurrentOption(0);

      // get the filtered array to select correct value
      const currentValue = filterArrayByQuery(value, options)[currentOption][1];

      // Simulate a click on the option
      onSelectOption(currentValue, fieldName);
    }

    // User pressed the up arrow, decrement the index
    if (evt.keyCode === 38) {
      if (currentOption === 0) return;

      setCurrentOption(currentOption - 1);
      scrollToElement();
    }

    // User pressed the down arrow, increment the index
    if (evt.keyCode === 40) {
      if (currentOption + 1 === filteredOptions.length) return;

      setCurrentOption(currentOption + 1);
      scrollToElement();
    }
  };

  // Set current item to index 0 every time options change
  useEffect(() => {
    if (options.length >= 1) {
      setCurrentOption(0);
    }
  }, [options]);

  return (
    <S.InputWrapper>
      {label && (
        <S.Label>
          {label} {isRequired && '*'}
        </S.Label>
      )}
      <S.StyledInput
        value={value}
        error={error}
        onChange={onChange}
        onBlur={onBlur}
        name={fieldName}
        type={type}
        onFocus={onFocus}
        onKeyDown={evt => onKeyDown(evt)}
      />
      {filteredOptions.length >= 1 && value.length >= 1 && isOpen && (
        <InputSuggestions
          currentOption={currentOption}
          options={filteredOptions}
          onSelectOption={onSelectOption}
          fieldName={fieldName}
        />
      )}
      {error && errorMessage && <S.ErrorMessage>{errorMessage}</S.ErrorMessage>}
    </S.InputWrapper>
  );
};

Input.defaultProps = {
  error: false,
  isOpen: false,
  isRequired: false,
  errorMessage: 'This inpput is required',
  label: '',
  onChange: () => {},
  onBlur: () => {},
  onFocus: () => {},
  onSelectOption: () => {},
  options: [],
};

Input.propTypes = {
  error: PropTypes.bool,
  isRequired: PropTypes.bool,
  isOpen: PropTypes.bool,
  errorMessage: PropTypes.string,
  label: PropTypes.string,
  options: PropTypes.array,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  type: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onSelectOption: PropTypes.func,
};

export default Input;
