import styled from 'styled-components';

export const InputWrapper = styled.div`
  width: 100%;
  float: left;
  margin-bottom: 20px;
  position: relative;
`;

export const Label = styled.label`
  width: 100%;
  float: left;
  margin-bottom: 10px;
  font-weight: 600;
`;

export const ErrorMessage = styled.div`
  width: 100%;
  float: left;
  margin-top: 15px;
  color: red;
`;

export const StyledInput = styled.input`
  width: 100%;
  float: left;
  display: block;
  width: 100%;
  height: 40px;
  color: rgb(77, 91, 101);
  cursor: auto;
  outline: none;
  border-width: 1px;
  border-style: solid;
  border-color: ${({error}) => (error ? 'red' : 'rgb(223, 227, 230)')};
  border-image: initial;
  padding: 6px 12px;
  font-size: 14px;

  &:focus {
    border-color: ${({error}) => (error ? 'red' : 'rgb(3, 71, 112)')};
  }

  &::-webkit-calendar-picker-indicator {
    display: none;
  }
`;
