import React from 'react';
import {mount} from 'enzyme';
import Input from './Input';

describe('Input', () => {
  let props = {
    name: 'city',
    type: 'text',
    value: 'Ber',
    options: [
      [10319, 'Berlin'],
      [15366, 'Neuenhagen bei Berlin'],
      [15562, 'Rüdersdorf bei Berlin'],
      [15566, 'Schöneiche bei Berlin'],
    ],
    isOpen: false,
  };

  let wrapper = mount(<Input {...props} />);

  it('renders without crashing given the required props and actions', () => {
    wrapper.find('input[name="city"]').simulate('focus');
    wrapper.find('input[name="city"]').simulate('keydown', {keyCode: 38});
    wrapper.find('input[name="city"]').simulate('blur');
    wrapper.find('input[name="city"]').simulate('change');

    expect(wrapper.html()).toMatchSnapshot();
  });

  it('renders without crashing given the required error props and actions', () => {
    props = {
      name: 'city',
      type: 'text',
      value: 'Ber',
      error: true,
      errorMessage: 'This field is required',
      options: [
        [10319, 'Berlin'],
        [15366, 'Neuenhagen bei Berlin'],
        [15562, 'Rüdersdorf bei Berlin'],
        [15566, 'Schöneiche bei Berlin'],
      ],
      isOpen: true,
    };

    wrapper = mount(<Input {...props} />);

    wrapper.find('input[name="city"]').simulate('focus');
    wrapper.find('input[name="city"]').simulate('keydown', {keyCode: 13});
    wrapper.find('input[name="city"]').simulate('keydown', {keyCode: 38});
    wrapper.find('input[name="city"]').simulate('keydown', {keyCode: 40});
    wrapper.find('input[name="city"]').simulate('keydown', {keyCode: 40});
    wrapper.find('input[name="city"]').simulate('keydown', {keyCode: 40});
    wrapper.find('input[name="city"]').simulate('keydown', {keyCode: 40});
    wrapper.find('input[name="city"]').simulate('keydown', {keyCode: 40});
    wrapper.find('input[name="city"]').simulate('keydown', {keyCode: 38});

    expect(wrapper.html()).toMatchSnapshot();
  });
});
