import React from 'react';
import ReactDOM from 'react-dom';
import WebFont from 'webfontloader';
import Layout from './components/Layout/Layout';
import UserDataForm from './containers/UserDataForm/UserDataForm';
import 'sanitize.css';
import './assets/styles/global.css';

WebFont.load({
  google: {
    families: ['Jaldi:300,400,400i,600,700,700i&display=optional'],
  },
});

const appRoot = (
  <Layout>
    <UserDataForm />
  </Layout>
);

ReactDOM.render(appRoot, document.getElementById('root'));
