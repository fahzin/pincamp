import {
  findCitiesByZipcode,
  filterArrayByQuery,
  verifyInput,
  verifyIfFormHasErrors,
} from './utils';

describe('findCitiesByZipcode function', () => {
  it('should filter by a zipcode (10409)', () => {
    const output = [[10409, 'Berlin']];
    expect(findCitiesByZipcode('10409')).toEqual(output);
  });

  it('should filter by a zipcode (12345) and return emopty array', () => {
    const output = [];
    expect(findCitiesByZipcode('12345')).toEqual(output);
  });
});

describe('filterArrayByQuery function', () => {
  it('should filter based on second elm of the array (Term: 1)', () => {
    const array = [
      [1, 'Option 1'],
      [2, 'Option 2'],
      [3, 'Option 3'],
      [4, 'Option 4'],
    ];
    const output = [[1, 'Option 1']];

    expect(filterArrayByQuery('1', array)).toEqual(output);
  });
});

describe('verifyInput function', () => {
  it('should return an Error with value empty', () => {
    const input = {
      value: '',
      validation: ['name'],
      isRequired: true,
    };

    const output = {
      error: true,
      errorMessage: 'This input is required',
      extraInfo: undefined,
    };

    expect(verifyInput(input)).toEqual(output);
  });

  it('should return no Error with value filled', () => {
    const input = {
      value: 'Fabio',
      validation: ['name'],
      isRequired: true,
    };

    const output = {
      error: false,
      errorMessage: undefined,
      extraInfo: undefined,
    };

    expect(verifyInput(input)).toEqual(output);
  });

  it('should return no Error with value empty and required false', () => {
    const input = {
      value: '',
      isRequired: false,
      validation: ['name'],
    };

    const output = {
      error: false,
      errorMessage: undefined,
      extraInfo: undefined,
    };

    expect(verifyInput(input)).toEqual(output);
  });

  it('should return Error with name validation, required and wrong value', () => {
    const input = {
      value: '123',
      isRequired: true,
      validation: ['name'],
    };

    const output = {
      error: true,
      errorMessage: 'Please, type a valid name',
      extraInfo: undefined,
    };

    expect(verifyInput(input)).toEqual(output);
  });

  it('should return an Error for zipcode non required but filled with less than 5 digits', () => {
    const input = {
      value: '1234',
      isRequired: false,
      validation: ['zipcode'],
    };

    const output = {
      error: true,
      errorMessage: 'Please, type a valid zipcode',
      extraInfo: undefined,
    };

    expect(verifyInput(input)).toEqual(output);
  });

  it('should return an Error for zipcode non required but filled with letters', () => {
    const input = {
      value: '12R4E',
      isRequired: false,
      validation: ['zipcode', 'number'],
    };

    const output = {
      error: true,
      errorMessage: 'This input accepts only numbers',
      extraInfo: undefined,
    };

    expect(verifyInput(input)).toEqual(output);
  });

  it('should return an Error for zipcode non required but wrongly filled', () => {
    const input = {
      value: '12345',
      isRequired: false,
      validation: ['zipcode'],
    };

    const output = {
      error: true,
      errorMessage: 'Please, enter a zipcode based in Germany',
      extraInfo: undefined,
    };

    expect(verifyInput(input)).toEqual(output);
  });
});

describe('verifyIfFormHasErrors function', () => {
  it('should return Error passing invalid data to required field', () => {
    const fields = {
      name: {
        value: 'Fabio',
        isRequired: false,
        validation: ['name'],
      },
      age: {
        value: '',
        isRequired: true,
        validation: ['number'],
      },
      password: {
        value: '12345',
        isRequired: false,
        validation: ['text'],
      },
    };

    const output = true;

    expect(verifyIfFormHasErrors(fields)).toEqual(output);
  });

  it('should return no Error passing valid data to required field', () => {
    const fields = {
      name: {
        value: 'Fabio',
        isRequired: false,
        validation: ['name'],
      },
      age: {
        value: '32',
        isRequired: true,
        validation: ['number'],
      },
      password: {
        value: '13089',
        isRequired: false,
        validation: ['zipcode'],
      },
    };

    const output = false;

    expect(verifyIfFormHasErrors(fields)).toEqual(output);
  });

  it('should return Error passing wrong data to non required field with verification if filled', () => {
    const fields = {
      name: {
        value: 'Fabio',
        isRequired: false,
        validation: ['name'],
      },
      age: {
        value: '32',
        isRequired: true,
        validation: ['number'],
      },
      zipcode: {
        value: '123',
        isRequired: false,
        validation: ['zipcode'],
      },
    };

    const output = true;

    expect(verifyIfFormHasErrors(fields)).toEqual(output);
  });
});
