import zicodesData from '../data/zipcodes.json';

// Find the cities based on the zipcode
export const findCitiesByZipcode = zipcode => {
  const citiesFound = zicodesData.filter(zip => zip[0] === Number(zipcode));
  return citiesFound;
};

// Create an array with unique city names
export const citiesList = zicodesData.filter(
  (set => res => !set.has(res[1]) && set.add(res[1]))(new Set())
);

// Return array of values including the query searched
export const filterArrayByQuery = (query, array) => {
  // Set to lowercase the query searched and the value
  // Create an array of results matching the query
  const filteredValues = array.filter(item =>
    item[1].toLowerCase().includes(query.toLowerCase())
  );

  // Return results including the query searched
  return filteredValues;
};

// Check if name is valid including surname if filled
export const nameIsValid = nameOrLastname => {
  const names = nameOrLastname.split(' ');
  const letters = /^[a-zA-Z-'’áéíóúâêôãõçÁÉÍÓÚÂÊÔÃÕÇüñÜÑüÜöÖäÄ]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/u;
  let valid = true;

  names.forEach(nameValue => {
    if (nameValue.match(letters) && valid) {
      valid = true;
    } else {
      valid = false;
    }
  });

  return valid;
};

// Function to check if input is valid
export const verifyInput = ({value, isRequired, validation}) => {
  // prevent user to send empty values
  value = value.trim();
  const isNumberRegx = /^\d+$/u;
  let error = false;
  let errorMessage;
  let extraInfo;

  if (value === '' && isRequired) {
    error = true;
    errorMessage = 'This input is required';
  } else if (
    validation.includes('name') &&
    !nameIsValid(value) &&
    value !== ''
  ) {
    error = true;
    errorMessage = 'Please, type a valid name';
  } else if (
    validation.includes('number') &&
    !isNumberRegx.test(value) &&
    value !== ''
  ) {
    error = true;
    errorMessage = 'This input accepts only numbers';
  } else if (
    value.length !== 5 &&
    validation.includes('zipcode') &&
    value !== ''
  ) {
    error = true;
    errorMessage = 'Please, type a valid zipcode';
  } else if (validation.includes('zipcode') && value.length === 5) {
    const cityFound = findCitiesByZipcode(value);

    if (
      cityFound !== null &&
      cityFound.length !== 0 &&
      cityFound.length === 1
    ) {
      extraInfo = {cityFound: cityFound[0][1]};
    } else {
      error = true;
      errorMessage = 'Please, enter a zipcode based in Germany';
    }
  }

  return {error, errorMessage, extraInfo};
};

// Function to check if Form has errors
export const verifyIfFormHasErrors = fields => {
  let formHasErrors = false;

  for (const key in fields) {
    if (fields.hasOwnProperty(key)) {
      // Get error status from each field
      const {error} = verifyInput({
        value: fields[key].value,
        validation: fields[key].validation,
        isRequired: fields[key].isRequired,
      });

      // Set error also if is not required but value is empty
      // (in case of field has special verifications when filled)
      if (
        (error && fields[key].isRequired) ||
        (error && !fields[key].isRequired && fields[key].value !== '')
      ) {
        formHasErrors = true;
      }
    }
  }

  return formHasErrors;
};
