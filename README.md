# PiNCAMP User Form

A ReactJS Application which the user can send a form with your data.

## Install & Usage

1. Install project dependencies, run the command below:

```bash
yarn
```

2. start the development server:

```bash
yarn start
```

## Run tests

> To run the project tests:

```bash
yarn test
```

The test will run with test coverage included.

## Run production build

> To create a production-ready build run following command:

```bash
yarn build
```

# Folder Structure

```
/public
  - favicon.ico
  - index.html
  - manifest.json
/ src
  / assets
    / images
    / styles
  / components
    / ComponentName
      / __snapshots__
      - ComponentName.js
      - componentName.styled.js
      - componentName.test.js
  / containers
    / ContainerName
      / __snapshots__
      - ContainerName.js
      - containerName.test.js
  / data
    - yourDataFile.json
  / shared
    - utils.js
    - utils.test.js
  - index.js
  - setupTests.js
```

> Is important to maintain the naming structure to make it easier to find files in the project and preventing import errors due to mismatch names. 

## Adding more fields to the form

> `/src/containers/UserFormData/UserFormData.js`

- The fields configuration of the Form lies in this file.
- The container was built in a way the fields can be fetched from a API in the future or switched to props to make it the most flexible possible.

To add extra fields add a new key on the object inside `fieldsData` const. The key of your field object will represent also the name of the field `city: { name: 'city' }` = `<input name="city" />`. Is important to maintain both is sync to prevent mismatch while calling functions like `onChange`. 

```js
// --- Example - fieldsData {}
const [fieldsData, setFieldsData] = useState({
  name: {
    label: 'Name',
    name: 'name',
    type: 'text',
    value: '',
    placeholder: 'Please, enter your full name',
    isRequired: true,
    error: false,
    errorMessage: '',
  },
});
```

- The form now accepts any kind of input type text based: [text, password, number, ...]
- You can also add an autocomplete option to the field, just pass an array of arrays, each array should follow the structure below:

```js
// --- Example - fieldsData {} with options [ID, VALUE]
const [fieldsData, setFieldsData] = useState({
  city: {
    label: 'City',
    name: 'city',
    type: 'text',
    value: '',
    placeholder: 'Please, enter your city',
    isRequired: true,
    options: [
      [10319, 'Berlin'],
      [15366, 'Neuenhagen bei Berlin'],
      [15562, 'Rüdersdorf bei Berlin'],
      [15566, 'Schöneiche bei Berlin'],
    ],
    isOpen: false,
    error: false,
    errorMessage: '',
  }
});
```

> `isOpen` just is considered if the user has focused on the input and has typed at least one letter into it.

## Adding validation state to the field

In order to validate fields, you have the key `validation: []` in the array you can specify what kind of rules may apply to the field. You can also combine validations linke in the example:

```js
// --- Example - fieldsData {} with combined validation []
const [fieldsData, setFieldsData] = useState({
  zipcode: {
    label: 'Zipcode',
    value: '',
    name: 'zipcode',
    type: 'text',
    placeholder: 'Please, enter your zipcode',
    isRequired: true,
    error: false,
    errorMessage: '',
    validation: ['zipcode', 'number'],
  }
});
```

This approach leave room for further implementations of different kinds of validation if needed.

## Adding error state to the field

In order to display error messages on the field just change error to true and add a string to errorMessage key:

```js
// --- Example - field {} with error BOOL and errorMessage String
setFieldsData(prevState => ({
  ...prevState,
  [fieldName]: {
    ...prevState[fieldName],
    error: true,
    errorMessage: 'This field is required.',
  },
}));
```
## Ejecting packages

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into the project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
